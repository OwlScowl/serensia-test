﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_serensia
{
    interface IAmTheTest
    {
        bool CheckSirenValidity(string siren);

        // Returns the full siren from the sirenWithoutControlNumber

        string ComputeFullSiren(string sirenWithoutControlNumber);
    }
}
