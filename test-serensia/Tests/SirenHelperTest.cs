﻿using NUnit.Framework;

namespace test_serensia.Tests
{
    [TestFixture]
    public class SirenHelperTest
    {
        [Test]
        [TestCase("732829320", TestName = "Good Siren, ending in 0", ExpectedResult = true)]
        [TestCase("552100554", TestName = "Good Siren, ending in 4", ExpectedResult = true)]
        [TestCase("73282932", TestName = "Siren wrong size", ExpectedResult = false)]
        [TestCase("73282932222", TestName = "Siren too big", ExpectedResult = false)]
        [TestCase("7328293z", TestName = "Siren with non-digit", ExpectedResult = false)]
        [TestCase("", TestName = "Empty Siren", ExpectedResult = false)]
        [TestCase(null, TestName = "Siren null", ExpectedResult = false)]
        public bool CheckSirenValidity(string siren)
        {
            var sh = new SirenHelper();
            return sh.CheckSirenValidity(siren);
        }

        [Test]
        [TestCase("73282932", TestName = "Good Siren, should find 0", ExpectedResult = "732829320")]
        [TestCase("55210055", TestName = "Good Siren, should end in 4", ExpectedResult = "552100554")]
        [TestCase("732", TestName = "Compute Siren wrong size", ExpectedResult = "")]
        [TestCase("73282932222", TestName = "Compute Siren too big", ExpectedResult = "")]
        [TestCase("7328293z", TestName = "Compute Siren with non-digit", ExpectedResult = "")]
        [TestCase("", TestName = "Compute Empty Siren", ExpectedResult = "")]
        [TestCase(null, TestName = "Compute Siren null", ExpectedResult = "")]
        public string ComputeFullSiren(string sirenWithoutControlNumber)
        {
            var sh = new SirenHelper();
            return sh.ComputeFullSiren(sirenWithoutControlNumber);
        }

    }
}
