﻿using System;
using System.Linq;

namespace test_serensia
{
    public class SirenHelper : IAmTheTest
    {
        private const int NB_SIREN_CONTROL_NUMBER = 9;
        private const int NB_SIREN_NO_CONTROL_NUMBER = 8;

        public SirenHelper()
        {
        }

        private bool IsStringOnlyDigits(string stringToTest)
        {
            return !stringToTest.Any(c => !char.IsDigit(c));
        }

        public bool CheckSirenValidity(string siren)
        {
            bool isSirenValid;
            if (!string.IsNullOrEmpty(siren))
            {
                if (siren.Length == NB_SIREN_CONTROL_NUMBER
                    && this.IsStringOnlyDigits(siren)
                    && this.GetSirenControlNumber(siren) == siren.Last().ToString())
                {
                    isSirenValid = true;
                }
                else
                {
                    Console.WriteLine($"siren not good length or contains a non digit char or control number is wrong : {siren}");
                    isSirenValid = false;
                }
            }
            else
            {
                Console.WriteLine($"siren is null or empty : {siren ?? "null"}");
                isSirenValid = false;
            }

            return isSirenValid;
        }

        public string ComputeFullSiren(string sirenWithoutControlNumber)
        {
            string fullSiren;

            if (!string.IsNullOrEmpty(sirenWithoutControlNumber))
            {
                if (sirenWithoutControlNumber.Length == NB_SIREN_NO_CONTROL_NUMBER
                    && this.IsStringOnlyDigits(sirenWithoutControlNumber))
                {
                    fullSiren = sirenWithoutControlNumber + this.GetSirenControlNumber(sirenWithoutControlNumber);
                }
                else
                {
                    Console.WriteLine($"siren not good length or contains a non digit char  : {sirenWithoutControlNumber}");
                    fullSiren = string.Empty;
                }
            }
            else
            {
                Console.WriteLine($"siren is null or empty : {sirenWithoutControlNumber ?? "null"}");
                fullSiren = string.Empty;
            }

            return fullSiren;
        }

        private string GetSirenControlNumber(string siren)
        {
            int total = 0;
            for (int i = 0; i < NB_SIREN_NO_CONTROL_NUMBER; i++)
            {
                var value = int.Parse(siren[i].ToString());
                total += this.GetSumOfElements(value * this.GetMultiplierValue(i));
            }
            return this.FindControlNumber(total);
        }

        private int GetMultiplierValue(int i)
        {
            int mutliplierValue;

            if (i % 2 == 0)
            {
                mutliplierValue = 1;
            }
            else
            {
                mutliplierValue = 2;
            }

            return mutliplierValue;
        }

        /// <summary>
        /// Makes the sum of all elements of a number (maximum 99)
        /// </summary>
        /// <example>21 => 2 + 1 = 3</example>
        /// <remarks>Value will never have more than two elements (max is 2 x 9)</remarks>
        /// <param name="value"></param>
        /// <returns></returns>
        private int GetSumOfElements(int value)
        {
            int sumOfElements;

            if (value < 10)
            {
                sumOfElements = value;
            }
            else
            {
                sumOfElements = value / 10 + value % 10;
            }

            return sumOfElements;
        }

        /// <summary>
        /// Returns control number of siren, 0 if it's dividable by 10, distance to 10 otherwise
        /// </summary>
        /// <param name="total"></param>
        /// <returns></returns>
        private string FindControlNumber(int total)
        {
            int controlNumber;
            if (total % 10 == 0)
            {
                controlNumber = 0;
            }
            else
            {
                controlNumber = 10 - total % 10;
            }

            return (controlNumber).ToString();
        }
    }
}